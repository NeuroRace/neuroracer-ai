from abc import abstractmethod

from neuroracer_common.version_solver import get_ABC

ABC = get_ABC()


class AbstractProcessor(ABC):
    """
    Abstract Processor base class.
    Derived class should only exist in the library! As it otherwise breaks the
    saving of the ai!
    """

    def __init__(self):
        pass

    @abstractmethod
    def process(self, data):
        """
        Processes the data and returns the processed data.

        :param data: data to process
        :type data: Union[Dict[str, np.ndarray], np.ndarray]
        :return: processed data
        :rtype: Union[Dict[str, np.ndarray], np.ndarray]
        """

        pass
