import os
from ruamel.yaml import safe_dump, safe_load

from neuroracer_ai.factories.abstract import EBackends

# Set base dir path given NR_HOME env variable, if applicable.
# Otherwise either ~/.neurorace or /tmp.
if 'NR_HOME' in os.environ:
    _nr_dir = os.environ.get('NR_HOME')
else:
    _nr_base_dir = os.path.expanduser('~')
    if not os.access(_nr_base_dir, os.W_OK):
        _nr_base_dir = '/tmp'
    _nr_dir = os.path.join(_nr_base_dir, '.neurorace')

# Default backend: keras.
_BACKEND = EBackends.KERAS

# Attempt to read config file.
_config_path = os.path.expanduser(os.path.join(_nr_dir, 'neurorace.yaml'))
if os.path.exists(_config_path):
    try:
        with open(_config_path) as f:
            _config = safe_load(f)
    except ValueError:
        _config = {}
    _BACKEND = _config.get('backend', _BACKEND)

# Save config file, if possible.
if not os.path.exists(_nr_dir):
    try:
        os.makedirs(_nr_dir)
    except OSError:
        # Except permission denied and potential race conditions
        # in multi-threaded environments.
        pass

if not os.path.exists(_config_path):
    _config = {
        'backend': _BACKEND,
    }
    try:
        with open(_config_path, 'w') as f:
            f.write(safe_dump(_config, indent=4))
    except IOError:
        # Except permission denied.
        pass

# Set backend based on NR_BACKEND flag, if applicable.
if 'NR_BACKEND' in os.environ:
    _backend = os.environ['NR_BACKEND']
    if _backend:
        _BACKEND = _backend


def get_backend():
    return _BACKEND
