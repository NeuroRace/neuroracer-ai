from abc import abstractmethod

from neuroracer_common.version_solver import get_ABC

ABC = get_ABC()


class AbstractGenerator(ABC):
    """
    Abstract base Class for Generator Classes. It defines all methods needed
    to be implemented for the class to be considered a Python Generator.
    """

    def __init__(self):
        self.index = 0

    @abstractmethod
    def send(self):
        raise NotImplementedError

    def throw(self, type=None, value=None, traceback=None):
        raise StopIteration

    def __iter__(self):
        return self

    def __next__(self):
        return self.send()

    next = __next__

    def close(self):
        """
        Raise GeneratorExit inside generator.
        """
        try:
            self.throw(GeneratorExit)
        except (GeneratorExit, StopIteration):
            pass
        else:
            raise RuntimeError("generator ignored GeneratorExit")
