from abc import ABCMeta, abstractmethod

import cv2

CV_LOAD_IMAGE_AS_IS = -1


class AbstractSnapshot:
    """
    Defines a snapshot representing a state and actions.
    The actions must be added by subclasses.
    """

    __metaclass__ = ABCMeta

    def __init__(self, action_msgs, timestamp):
        """
        Initiates a Snapshot.

        :param action_msgs: A dictionary specifying the action's values.
        :type action_msgs: dict
        :param timestamp: The timestamp when the snapshot was recorded.
        :type timestamp: int
        """

        self.action_msgs = action_msgs
        self.timestamp = timestamp

    # hack to get the system working with only steer as return value
    # def get_actions_as_tupel(self, keys=("steer", "drive")):
    # TODO change in train.py
    def get_actions_as_tupel(self, keys=("steer",)):
        """
        Returns values as tuple in this event class.

        :param keys: A tuple of keys defining the order of the returned tuple
        :type keys: tuple
        :returns: A tuple of actions in the same order as keys.
        :rtype: tuple
        """

        value_list = []
        for key in keys:
            value_list.append(self.action_msgs[key])

        return tuple(value_list)

    def get_nb_actions(self):
        """
        Returns the number of actions in this event class.

        :returns: The number of actions in this event class.
        :rtype: int
        """

        return len(self.action_msgs)

    @abstractmethod
    def update_snapshot(self, actions, *args):
        """
        TODO

        :param actions: TODO
        :param args: TODO
        :return: TODO
        """
        raise NotImplementedError


class ImageSnapshot(AbstractSnapshot):
    """
    Defines a snapshot with arbitrary amount of camera messages and
    action messages associated
    """

    def __init__(self, camera_msgs, action_msgs, timestamp):
        """
        Initiates a ImageSnapshot.

        :param action_msgs: The actions for this Snapshot.
        :type action_msgs: dict[ros_topic: ros_msg]
        :param camera_msgs:The Camera messages for this snapshot.
        :type camera_msgs: dict[ros_topic: ros_msg]
        :param timestamp: The timestamp when the snapshot was recorded.
        :type timestamp: int
        """

        AbstractSnapshot.__init__(self, action_msgs, timestamp)

        if not camera_msgs:
            raise ValueError("camera message dict cannot be empty")

        self.camera_msgs = camera_msgs

    @staticmethod
    def copy(other, camera_msgs=None, action_msgs=None, timestamp=None):
        """
        Returns a new ImageSnapshot that is a copy of "other", where every
        attribute that is not None is overwritten with the value of the
        associated parameter.

        :param other: TODO
        :type other: TODO
        :param camera_msgs: The camera msgs to copy
        :type camera_msgs: dict
        :param action_msgs: The actions to copy
        :type action_msgs: dict
        :param timestamp: The timestamp when the snapshot was recorded.
        :type timestamp: dict
        :return: A copy of other with the given values.
        :rtype: ImageSnapshot
        """

        camera_msgs = camera_msgs if camera_msgs is not None else other.camera_msgs
        action_msgs = action_msgs if action_msgs is not None else other.action_msgs
        timestamp = timestamp if timestamp is not None else other.timestamp

        return ImageSnapshot(camera_msgs=camera_msgs,
                             action_msgs=action_msgs,
                             timestamp=timestamp)

    def get_uncompressed_camera_msgs(self):
        """
        Returns the uncompressed camera msgs (images) of this ImageSnapshot.

        :returns: The uncompressed images of this ImageSnapshot.
        :rtype: dict[ros_topic: ros_msg]
        """

        camera_uncompressed = {}

        for topic, img_msg in self.camera_msgs.items():
            decoded = cv2.imdecode(img_msg, flags=CV_LOAD_IMAGE_AS_IS)
            img = decoded if decoded is not None else img_msg
            camera_uncompressed[topic] = img

        return camera_uncompressed

    def update_snapshot(self, actions, *args):
        pass
