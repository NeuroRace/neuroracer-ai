"""
Everything which belongs to the keras backend.
"""
from __future__ import division

import os
import warnings

from neuroracer_ai.models.abstract import AbstractModel, Debuggable

# hide keras future warnings as we can't change them and they bleed into our output
with warnings.catch_warnings():
    warnings.simplefilter(action="ignore", category=FutureWarning)

    import keras as k
    import vis.utils.utils as kvu
    import vis.visualization as kv


class KerasBaseModel(AbstractModel, Debuggable):
    """
    Keras based implementation.

    NOTE: THIS MODEL CLASS ONLY SUPPORTS EXECUTIVE TASKS; NOT USABLE FOR TRAINING
    """

    def __init__(self):
        super(KerasBaseModel, self).__init__()

        # common attributes
        self._internal_model = None  # type: km.Model
        self._is_regression = True

        # Keras specific attributes

    def create(self, architecture_func, architecture_func_params, is_regression=True, print_summary=True):
        """
        Creates a new model based on the given architecture_func and its
        parameters.

        :param architecture_func: architecture construction function
        :type architecture_func: Callable[[**kwargs], keras.Model]
        :param architecture_func_params: parameters which get passed to to the
                architecture construction function
        :type architecture_func_params: Dict[str, Any]
        :param is_regression: Whether the problem is regression or classification based
        :type is_regression: bool
        :param print_summary: defines if the summary of the model gets printed
                              at the end
        :type print_summary: bool
        :return: created Model instance
        :rtype: KerasBaseModel
        """

        self._internal_model = architecture_func(**architecture_func_params)

        self._is_regression = is_regression

        if print_summary:
            self._internal_model.summary()

        return self

    def save(self, model_path, overwrite=True):
        """
        Saves the model at the given path.

        :param model_path: save path (consists of dir + file)
        :type model_path: str
        :param overwrite: manages if model files should be overwritten
        :type overwrite: bool
        """

        self._internal_model.save(filepath=os.path.expanduser(model_path), overwrite=overwrite)

    def load(self, model_path, trainable=True):
        """
        Tries to load the given file and returns a new instance of this object
        if it is successful. Trainable defines if you can train a model after
        loading. If you try to train an untrainable model an Error will be
        thrown.

        :param model_path: TODO
        :type model_path: str
        :param trainable: loads the model in a way which allows it
                          to be trained.
        :type trainable: bool
        :return created Model instance
        :rtype: keras.KerasModel
        :raises IOError: if loading of the model fails
        """

        try:
            self._internal_model = k.models.load_model(os.path.expanduser(model_path), compile=trainable)
            return self

        except ValueError as e:
            raise IOError(
                "Trying to load the model ({}) failed!\nMessage: {}".format(model_path, e))

    def predict(self, input_data, verbose=0):
        """
        Predicts output based on the model.

        :param input_data: TODO
        :type input_data: numpy.ndarray
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: TODO
        :rtype: numpy.ndarray
        """

        return self._internal_model.predict(input_data, verbose=verbose)

    def _resolve_layer_idx(self, layer_name, layer_idx):
        """
        Resolves layer by given name or index

        :param layer_name: TODO
        :param layer_idx: TODO
        :return: TODO
        """
        idx = layer_idx
        if layer_name:
            idx = kvu.find_layer_idx(self._internal_model, layer_name)

        return idx

    def visualize_activation(self, layer_name=None, layer_idx=None, filter_indices=None,
                             seed_input=None):
        """
        Visualizes the activation of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        """
        idx = self._resolve_layer_idx(layer_name=layer_name, layer_idx=layer_idx)

        return kv.visualize_activation(
            model=self._internal_model,
            layer_idx=idx,
            filter_indices=filter_indices,
            seed_input=seed_input,
            backprop_modifier="guided")

    def visualize_saliency(self, layer_name=None, layer_idx=None, filter_indices=None,
                           seed_input=None):
        """
        Visualizes the saliency of the nn and returns it as a numpy array.

        :param layer_name: name of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_name: str
        :param layer_idx: index of the layer to visualize (only name or idx is
                needed, if both is supplied idx has priority)
        :type layer_idx: int
        :param filter_indices: list of indices from filters which get
                visualized. if none all are visualized
        :type filter_indices: List[int]
        :param seed_input: list of images which help the visualization to
                converge
        :type seed_input: np.ndarray
        :return: visualization
        :rtype: np.ndarray
        """
        idx = self._resolve_layer_idx(layer_name=layer_name, layer_idx=layer_idx)

        return kv.visualize_saliency(
            model=self._internal_model,
            layer_idx=idx,
            filter_indices=filter_indices,
            seed_input=seed_input,
            backprop_modifier="guided")

    def get_weights(self, layer_name=None, layer_idx=None):
        """
        Retrieves a layer's weights based on either its name (unique) or index.

        If `name` and `index` are both provided, `index` will take precedence.

        :param layer_name: TODO
        :param layer_idx: TODO
        :return: TODO
        """
        layer = self._internal_model.get_layer(name=layer_name, index=layer_idx)

        if layer is None:
            return None

        return layer.get_weights()

    def get_summary(self):
        """
        Retrieves a model's summary.

        :return: TODO
        """
        summary_list = []

        def append_lines(line):
            if len(summary_list) != 0:
                summary_list.append("\n")
            summary_list.append(line)

        self._internal_model.summary(print_fn=append_lines)
        return "".join(summary_list)
