"""
Contains the AbstractModel interface and all implementations of it. Available
architectures for the models are also in here. This servers as a abstraction
from the used framework.
"""

from neuroracer_ai.factories.abstract import EBackends
from neuroracer_ai.models.abstract import AbstractModel, Debuggable, Trainable
from neuroracer_ai.utils.backend_solver import get_backend

imports = [
    "AbstractModel",
    "Debuggable",
    "Trainable"
]
_BACKEND = get_backend()

if _BACKEND == EBackends.KERAS:
    try:
        from neuroracer_ai.models.keras_base_backend import KerasBaseModel

        imports.append("KerasBaseModel")
    except ImportError as e:
        print("Required module for defined backend Keras not found:", e)

elif _BACKEND == EBackends.PYTORCH:
    try:
        from neuroracer_ai.models.pytorch_base_backend import PyTorchBaseModel

        imports.append("PyTorchBaseModel")
    except ImportError as e:
        print("Required module for defined backend PyTorch not found:", e)

else:
    raise NotImplementedError('Define backend not found: ', _BACKEND)

__all__ = imports
