from abc import abstractmethod

import numpy as np

from neuroracer_ai.suites import AbstractProcessorSuite


class SnapshotProcessorSuite(AbstractProcessorSuite):
    """
    Processes an ImageSnapshot object. Since snapshots contain more information
    than is usually needed for training a model, we need to extract the
    relevant information depending on the model type and architecture.
    """

    def __init__(self):
        super(SnapshotProcessorSuite, self).__init__()

    @abstractmethod
    def process(self, *args, **kwargs):
        raise NotImplementedError

    @staticmethod
    def transform_action(action_msg):
        # TODO ISSUE #9: https://gitlab.com/NeuroRace/neuroracer-ai/issues/9
        # TODO problem caused by _get_snapshot_data_from_sub_dir() in extracted_data_reader.py
        # TODO cast could be done here because action_msg is a CommentedMap and not a dict
        # if not isinstance(action_msg, dict):
        #    action_msg = convert_ros_message_to_dictionary(action_msg)

        # TODO improve and remove Vector3!?
        # Robot:
        # Twist
        if "linear" in action_msg and "angular" in action_msg:
            drive = action_msg.get("linear").get("x")
            steer = action_msg.get("angular").get("z")

        # Vector3
        elif "x" in action_msg and "z" in action_msg:
            drive = action_msg.get("x")
            steer = action_msg.get("z")

        # Simulation:
        # Ackermann
        elif "drive" in action_msg:
            drive = action_msg.get("drive").get("speed")  # desired forward speed(m/s)
            steer = action_msg.get("drive").get("steering_angle")  # desired virtual angle (radians)
            # TODO further parameters
            # jerk: default 0.0 (instant), desired jerk (m/s^3)
            # acceleration: default 0.0 (instant), desired acceleration (m/s^2)
            # steering_angle_velocity: default 0.0 (instant), desired rate of change (radians/s)

        else:
            raise ValueError("Action Message must contain Drive and Steer information"
                             "as msg.x, msg.z or msg.linear.x, msg.angular.z ")

        return drive, steer


class SingleViewSnapshotProcessorSuite(SnapshotProcessorSuite):
    """
    Use for models which expect one image as Input and predict only
    a steering angle.
    """

    def __init__(self, camera_topic=None, action_topics=None):
        super(SingleViewSnapshotProcessorSuite, self).__init__()
        self.camera_topic = str(camera_topic[0]) if camera_topic else "/left/image_raw_color/compressed"
        self.action_topic = str(action_topics[0]) if action_topics else "/nr/engine/input/actions"

    def process(self, snapshot_batch, use_multithreading=False):
        x, y = [], []

        for snapshot in snapshot_batch:
            image = snapshot.get_uncompressed_camera_msgs()[self.camera_topic]

            action_msg = snapshot.action_msgs[self.action_topic]
            drive, steer = self.transform_action(action_msg)

            x.append(np.array([image]))
            y.append((steer,))

        return np.array(x), np.array(y)


class DualViewSnapshotProcessorSuite(SnapshotProcessorSuite):
    """
    Use for models which take two zed images (left/right) as input and predict
    only a steering angle.
    """

    def __init__(self, camera_topics=None, action_topics=None):
        super(DualViewSnapshotProcessorSuite, self).__init__()
        self.camera_left_topic = str(camera_topics[0]) if camera_topics[0] else "/left/image_raw_color/compressed"
        self.camera_right_topic = str(camera_topics[1]) if camera_topics[1] else "/right/image_raw_color/compressed"
        self.action_topic = str(action_topics[0]) if action_topics[0] else "/nr/engine/input/actions"

    def process(self, snapshot_batch, use_multithreading=False):
        x, y = [], []

        for snapshot in snapshot_batch:
            uncompressed = snapshot.get_uncompressed_camera_msgs()
            left_image = uncompressed[self.camera_left_topic]
            right_image = uncompressed[self.camera_right_topic]

            action_msg = snapshot.action_msgs[self.action_topic]
            drive, steer = self.transform_action(action_msg)

            x.append(np.array([left_image, right_image]))
            y.append(steer)

        return np.array(x), np.array(y)
