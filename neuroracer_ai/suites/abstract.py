from abc import abstractmethod

from neuroracer_common.version_solver import get_ABC

from neuroracer_ai.processors.abstract import AbstractProcessor

ABC = get_ABC()


class AbstractProcessorSuite(AbstractProcessor, ABC):
    """
    Abstract ProcessorSuite base class.

    Derived class should only exist in the library! As it otherwise breaks the
    saving of the ai!
    """

    @abstractmethod
    def process(self, data):
        """
        Processes the data and returns the processed data.

        :param data: data to process
        :type data: Union[Dict[str, np.ndarray], np.ndarray]
        :return: processed data
        :rtype: Union[Dict[str, np.ndarray], np.ndarray]
        """

        pass
