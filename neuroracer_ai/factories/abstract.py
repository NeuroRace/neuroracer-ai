from abc import abstractmethod

from neuroracer_common.version_solver import get_ABC

ABC = get_ABC()


class EBackends:
    """
    Enum for the different implemented backends, like keras or pytorch

    DON'T inherit enum.Enum
    because with enum.Enum default (EBackends.KERAS == "keras") is False
    and we want this behavior, because of our yaml files
    """

    def __init__(self):
        pass

    KERAS = "keras"
    PYTORCH = "pytorch"


class AbstractBackendFactory(ABC):
    """
    Abstract backend factory interface. It abstracts the raw backend factory from the
    user as well as the rest of the lib.
    """

    def __init__(self):
        pass

    @staticmethod
    @abstractmethod
    def build():
        """
        Build method to get an implementation of a backend

        :return: backend instance
        :type:   AbstractModel
        """
        pass
